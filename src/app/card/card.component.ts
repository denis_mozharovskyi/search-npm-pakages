import {Component, ComponentFactoryResolver, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {ApiJsdelivrService, Card, PackageVersions} from "../services/api-jsdelivr.service";
import {ModalComponent} from "../modal/modal.component";
import {RefDirective} from "../ref.directive";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnChanges {
  @Input() strQuery: any;
  loading = false;
  IsCardVariants = false;
  packageName = '';
  cards: Card[] = [];
  error = '';
  pageActive = 1;
  packageVer: any;
  @ViewChild(RefDirective) refDir!: RefDirective;

  constructor(private cardServise: ApiJsdelivrService, private resolver: ComponentFactoryResolver) { }

  ngOnChanges(changes: SimpleChanges){
    if(!this.strQuery.trim()) return;
    this.searchNamePackage(this.strQuery, 'npm');
  }

  ngOnInit() {
    this.getPopular();
  }

  getPopular(){
    this.loading = true;
    this.cardServise.getPopularPackages().subscribe( cards => {
      this.cards = cards;
      this.loading = false;
    }, err => {
      this.error = err.message;
    });
  }

  getPage(i: number) {
    this.pageActive = i;
    this.loading = true;
    this.cardServise.getPage(i).subscribe(cards => {
      this.cards = cards;
      this.loading = false;
    }, err => {
      this.error = err.message;
    });
  }

  searchNamePackage(name: string, type: string){
    this.loading = true;
    this.packageName = name;
    this.cardServise.searchPackage(name, type).subscribe(p =>{
      this.packageVer = p;
      this.loading = false;
      this.IsCardVariants = true;
    }, err => {
      this.error = err.message;
    });
  }

  showModal(version: string){
      const modalFactory = this.resolver.resolveComponentFactory(ModalComponent);
      this.refDir.containerRef.clear();
      const component = this.refDir.containerRef.createComponent(modalFactory);
      this.loading = true;
      this.cardServise.getPackageVersion(version).subscribe( res => {
        component.instance.jsonObject = res;
        component.instance.close.subscribe(() => {
          this.refDir.containerRef.clear();
        });
        this.loading = false;
      }, err => {
        this.error = err.message;
      });
  }

}
