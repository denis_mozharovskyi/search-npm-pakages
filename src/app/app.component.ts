import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  stringSearch = '';

  searchPackage(search: string){
    this.stringSearch = search;
  }
}
