import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  stringSearch = '';

  @Output() onSearch: EventEmitter<string> = new EventEmitter<string>();

  search(){
    console.log("string query", this.stringSearch);
    if (!this.stringSearch.trim()) return;
    this.onSearch.emit(this.stringSearch);
  }
}
