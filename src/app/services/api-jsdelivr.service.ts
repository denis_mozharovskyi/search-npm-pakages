import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

export interface Card {
  type: string;
  name: string;
  hits: number;
}

export interface PackageVersions {
  tags: {
    bets?: string;
    latest?: string;
  }
  versions: string[];
}

export interface InfoPackageVersion {

}

@Injectable({
  providedIn: 'root'
})
export class ApiJsdelivrService {

  activeUriPage = '';
  activeUriVariant = '';
  uri = 'https://data.jsdelivr.com/v1';

  constructor(private http: HttpClient) { }

  getPopularPackages(): Observable<Card[]> {
    this.activeUriPage = `${this.uri}/stats/packages?limit=10`;
    return this.http.get<Card[]>(this.activeUriPage);
  }

  getPage(page: number): Observable<Card[]>{
    return this.http.get<Card[]>(`${this.activeUriPage}&page=${page}`);
  }

  searchPackage(str: string, type = 'npm'): Observable<PackageVersions> {
    this.activeUriVariant = `${this.uri}/package/${type}/${str}`;
    return this.http.get<PackageVersions>(this.activeUriVariant);
  }

  getPackageVersion(version: string){
    return this.http.get(`${this.activeUriVariant}@${version}`);
  }
}
